set nowrap
set ignorecase
set viminfo='100,f1,:10,/10,%
set history=100
set guioptions+=b
set relativenumber 
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set hlsearch
set incsearch

set guifont=Menlo:h14
"""""""""""""""""""""""""""""" 
" Status line 
""""""""""""""""""""""""""""""
" Always show the status line 
set laststatus=2 
" Format the status line 
set statusline=\ %f\ \ PWD:\ %r%{getcwd()}%h\ %<\ %h%m%r%=%-14.(%l,%v%)\ %P 

